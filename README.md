#  MediaLibrary Integreation - GSoC 2018

Welcome to the landing page of the [VLCMediaLibraryKit integration project for GSoC 2018][1] with the VideoLAN organisation.

The overall goal was to integrate VLCMediaLibraryKit to VLC-iOS and remove the usage of the old MediaLibraryKit.

You can find below both of the repository I worked on during the summer of 2018:

https://code.videolan.org/GSoC2018/bubu/vlc-ios/commits/master

https://code.videolan.org/GSoC2018/bubu/VLCMediaLibraryKit/commits/master

## Contributions

### VLCMediaLibraryKit:

* Update VLCMediaLibraryKit to changes of the [VideoLAN medialibrary][2]
* Various VLCMediaLibraryKit improvements

### VLC-iOS:

* Base architecture changes
* File adding discovery by Wi-Fi and cloud services
* File sorting
* File deletion
* File renaming
* Playlist creation

## Todo

* Drag and drop re-integration
* Migration of the old database

[1]: https://summerofcode.withgoogle.com/projects/#6654427885207552
[2]: https://code.videolan.org/videolan/medialibrary
